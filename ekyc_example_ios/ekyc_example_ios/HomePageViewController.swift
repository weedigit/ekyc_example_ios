//
//  HomePageViewController.swift
//  ekyc_example_ios
//
//  Created by LV on 7/28/20.
//  Copyright © 2020 WEE DIGITAL. All rights reserved.
//

import Foundation
import UIKit
import EkycFramework

class HomePageViewController: UIViewController {
    @IBOutlet weak var lblHome: UILabel!
    @IBOutlet weak var optionView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
    }
    
    func setupUI() {
        self.setupText()
        self.setupView()
    }
    
    func setupView() {
        optionView.layer.shadowColor = UIColor.black.cgColor
        optionView.layer.shadowOpacity = 0.2
        optionView.layer.shadowOffset = CGSize(width: 0, height: 4)
        optionView.layer.shadowRadius = 40
    }
    
    func setupText() {
        let text = NSMutableAttributedString()
        text.append(NSAttributedString(string: "KNOW\nYOUR", attributes: [NSAttributedString.Key.foregroundColor: UIColor.black]))
        text.append(NSAttributedString(string: "\nFUTURE\n", attributes: [NSAttributedString.Key.foregroundColor: UIColor(hexString: "2D98F0")]))
        text.append(NSAttributedString(string: "CUSTOMER", attributes: [NSAttributedString.Key.foregroundColor: UIColor.black]))
        self.lblHome.attributedText = text
    }
    
    @IBAction func onTapEkycCamera(_ sender: Any) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ViewController") as? ViewController
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    @IBAction func onTapEkycLoginPincode(_ sender: Any) {
        self.useEkycLoginPinController()
    }
    
    @IBAction func onTapRegisterPincode(_ sender: Any) {
       //        self.useEkycPinView()
        self.useEkycRegisterPinController()
    }
    
    func useEkycPinView() {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PincodeViewController") as? PincodeViewController
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    func useEkycLoginPinController() {
        let vc = WELoginPinController()
        vc.pincodeDelegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func useEkycRegisterPinController() {
        let vc = WERegisterPinController()
        vc.pincodeDelegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

extension HomePageViewController: PincodeDelegate {
    func onTapBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func pincodeData(_ data: String) {
        DispatchQueue.main.async {
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PincodeResultViewController") as? PincodeResultViewController
            vc?.pinCode = data
            self.navigationController?.pushViewController(vc!, animated: true)
        }
    }
}
