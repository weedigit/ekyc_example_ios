//
//  PincodeResultViewController.swift
//  ekyc_example_ios
//
//  Created by LV on 7/29/20.
//  Copyright © 2020 WEE DIGITAL. All rights reserved.
//

import UIKit

class PincodeResultViewController: UIViewController {
    @IBOutlet weak var lblPinResult: UILabel!
    var pinCode: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
    }
    
    func setupUI() {
        lblPinResult.text = pinCode
    }
    
    @IBAction func onTapDone(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
}
