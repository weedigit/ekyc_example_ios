//
//  ViewController.swift
//  ekyc_example_ios
//
//  Created by LV on 6/23/20.
//  Copyright © 2020 WEE DIGITAL. All rights reserved.
//

import UIKit
import EkycFramework
import AVFoundation

class ViewController: UIViewController {
    var cameraPermissionActive: Bool = false
    var firstStartCamera: Bool = true
    
    lazy var ekycView:WEFaceView = {
        let bundle = Ekyc.getBundle(_class: ViewController.self)
        let nib = UINib(nibName: "WEFaceView", bundle: bundle)
        let customView = nib.instantiate(withOwner: self, options: nil).first as! WEFaceView
        let screenSize: CGRect = UIScreen.main.bounds
        customView.frame = CGRect(x: 0, y: 0, width: screenSize.width, height: screenSize.height)
        customView.extractDataDelegate = self
        
        return customView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        checkPermission()
        self.view.addSubview(self.ekycView)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        if cameraPermissionActive && firstStartCamera {
            self.firstStartCamera = false
            self.ekycView.startCamera()
        }
    }
    
    func checkPermission() {
        let status = AVCaptureDevice.authorizationStatus(for: .video)
        if status == .authorized {
            // camera access
            cameraPermissionActive = true
        } else {
            AVCaptureDevice.requestAccess(for: .video) { (granted: Bool) in
                DispatchQueue.main.async {
                    if granted {
                        self.cameraPermissionActive = true
                        self.ekycView.startCamera()
                    } else {
                        let statusRecheck = AVCaptureDevice.authorizationStatus(for: .video)
                        if statusRecheck == .authorized {
                        } else if statusRecheck == .denied || statusRecheck == .restricted {
                            self.requestPermissionAction()
                        }
                    }
                }
            }
        }
    }
    
    func requestPermissionAction() {
        //Camera permission
        let alertController = UIAlertController (title: "Camera Permission", message: "Go to Settings?", preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
            
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                return
            }
            
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                    print("Settings opened: \(success)") // Prints true
                })
            }
        }
        alertController.addAction(settingsAction)
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
    }
}

extension ViewController: ExtractFaceDelegate {
    func livenessCheckFace(_ status: ExtractFaceStatus) {
        switch status {
        case .COMPLETE(let resp):
            DispatchQueue.main.async {
                let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "FaceResultViewController") as? FaceResultViewController
                vc?.faceData = resp.face
                vc?.faceResultViewControllerDelegate = self
                self.navigationController?.pushViewController(vc!, animated: true)
            }
            break
        }
    }
    
    func onTapBack() {
        self.navigationController?.popViewController(animated: true)
    }
}

extension ViewController: FaceResultViewControllerDelegate {
    func onTapTry() {
        if cameraPermissionActive {
            self.ekycView.resetUI()
            self.ekycView.resetFlowCheckFace()
            self.ekycView.resetCamera()
        }
    }
}
