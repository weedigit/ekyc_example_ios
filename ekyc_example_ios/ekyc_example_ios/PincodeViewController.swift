//
//  PincodeViewController.swift
//  ekyc_example_ios
//
//  Created by LV on 6/25/20.
//  Copyright © 2020 WEE DIGITAL. All rights reserved.
//

import Foundation
import EkycFramework
import UIKit

class PincodeViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.moveToEkycPin()
    }
    
    func moveToEkycPin() {
        let bundle = Ekyc.getBundle(_class: PincodeViewController.self)
        let nib = UINib(nibName: "WEPinView", bundle: bundle)
        let customView = nib.instantiate(withOwner: self, options: nil).first as! WEPinView
        
        let screenSize: CGRect = UIScreen.main.bounds
        customView.frame = CGRect(x: 0, y: 0, width: screenSize.width, height: screenSize.height)
        customView.pincodeDelegate = self
        customView.pincodeConfig = PincodeConfig(type: PincodeType.login, pinNum: PincodeNumber.pin6, showBackButton: true, imgLogo: nil, imgButtonBack: nil, language: LanguageConfig.vi)
        self.view.addSubview(customView)
    }
}

extension PincodeViewController: PincodeDelegate {
    func onTapBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func pincodeData(_ data: String) {
        DispatchQueue.main.async {
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PincodeResultViewController") as? PincodeResultViewController
            vc?.pinCode = data
            self.navigationController?.pushViewController(vc!, animated: true)
        }
    }
}
