//
//  FaceResultViewController.swift
//  ekyc_example_ios
//
//  Created by LV on 7/29/20.
//  Copyright © 2020 WEE DIGITAL. All rights reserved.
//

import Foundation
import UIKit

protocol FaceResultViewControllerDelegate: class {
    func onTapTry()
}

class FaceResultViewController: UIViewController {
    @IBOutlet weak var imgFaceResult: UIImageView!
    var faceData: Data = Data()
    weak var faceResultViewControllerDelegate: FaceResultViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    func setupUI() {
        imgFaceResult.image = UIImage(data: faceData)
    }
    
    @IBAction func onTapBack(_ sender: Any) {
        self.faceResultViewControllerDelegate?.onTapTry()
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onTapDone(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
}
